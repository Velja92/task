<?php
include_once 'classes/engine.php';
include_once 'classes/car.php';
include_once 'classes/truck.php';
include_once 'classes/parking.php';

    $carEngine = new Engine(120, 'v8');
    $car = new Car('VW', 'Polo', 2009, 'car123', $carEngine);
    $car->showDescription();
    echo $car->engineSpecs();
    echo '<br><br>';

    $truckEngine = new Engine(200, 'v12');
    $truck = new Truck('Volvo', 'CargoTruck', 2005, 'truck123', 17, $truckEngine);
    $truck->showDescription();
    echo '<br><br>';

    $parking = Parking::getParking();
    $parking->parkCar($car);
    echo '<br><br>';
    $parking->parkCar($car);
    echo '<br><br>';
    $parking->parkCar($truck);
    echo '<br><br>';
    $parking->getCar($car);
    echo '<br><br>';
    $parking->getCar($car);
    echo '<br><br>';

