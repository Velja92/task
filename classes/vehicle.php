<?php

abstract class Vehicle{

    abstract public function getModel();
    abstract public function getType();
    abstract public function getAge();
    abstract public function getLicencePlate();

    public function showDescription(){
        printf('This is a %s %s made in %d with a licence plate %s! ',
            $this->getType(), $this->getModel(), $this->getAge(), $this->getLicencePlate());
    }
}