<?php

include_once 'vehicle.php';
include_once 'engine.php';

class Truck extends Vehicle{

    private $type;
    private $model;
    private $age;
    private $engine;
    private $licencePlate;
    private $cargo = 8;

    function __construct(string $type, string $model, int $age, string $licencePlate, int $cargo, Engine $engine){
        $this->type = $type;
        $this->model = $model;
        $this->age  = $age;
        $this->licencePlate  = $licencePlate;
        $this->cargo  = $cargo;
        $this->engine = $engine;
    }

    public function getAge(){
        return $this->age;
    }

    public function getModel(){
        return $this->model;
    }

    public function getType(){
        return 'truck ' . $this->type;
    }

    public function getLicencePlate(): string{
        return $this->licencePlate;
    }

    public function setLicencePlate(string $Lp){
        $this->licencePlate = $Lp;
    }

    public function engineSpecs(){
        return $this->engine->getDescription();
    }

    public function showDescription()
    {
        parent::showDescription();
        printf ('It can carry up to %d tons!', $this->cargo);
    }
}