<?php

class Engine{
    private $hp;
    private $type;

    function __construct(int $horsePower, string $type){
        $this->hp   = $horsePower;
        $this->type = $type;
    }

    public function getHp(): int {
        return $this->hp;
    }

    public function getType(): string {
        return $this->type;
    }

    public function getDescription(){
        printf('The engine is %s with %dHP!', $this->type, $this->hp );
    }
}
