<?php

class Parking{

    private static $parking = NULL;
    private $cars = array();

    private function __construct(){
    }

    static function getParking(){
        if(self::$parking == NULL){
            self::$parking = new Parking();
        }
        return self::$parking;
    }

    public function listCars(): array{
        return $this->cars;
    }

    public function parkCar(Vehicle $vehicle){
        $id = $vehicle->getLicencePlate();
        if(!in_array($vehicle, $this->cars)){
            $this->cars[$id] = $vehicle;
            printf ('The vehicle %s is successfully parked!', $id);
        }
        else{
            printf ('The vehicle %s is already parked!', $id);
        }
    }

    public function getCar(Vehicle $vehicle){
        $id = $vehicle->getLicencePlate();
        if(in_array($vehicle, $this->cars)){
            unset($this->cars[$id]);
            printf ('The vehicle %s is successfully unparked!', $id);
        }
        else{
            print 'There is no such vehicle on the lot!';
        }
    }

}