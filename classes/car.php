<?php

include_once 'vehicle.php';
include_once 'engine.php';

class Car extends Vehicle{

    private $type;
    private $model;
    private $age;
    private $engine;
    private $licencePlate;

    function __construct(string $type, string $model, int $age, string $licencePlate, Engine $engine){
        $this->type = $type;
        $this->model = $model;
        $this->age  = $age;
        $this->licencePlate  = $licencePlate;
        $this->engine = $engine;
    }

    public function getAge(): int{
        return $this->age;
    }

    public function getModel(): string{
        return $this->model;
    }

    public function getType(): string{
        return 'car ' . $this->type;
    }

    public function getLicencePlate(): string{
        return $this->licencePlate;
    }

    public function setLicencePlate(string $Lp){
        $this->licencePlate = $Lp;
    }

    public function engineSpecs(){
        return $this->engine->getDescription();
    }
}